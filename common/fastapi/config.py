from __future__ import annotations

from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from common.fastapi.base import AppBaseComponent


class Config:
    """
    Config class for our application. Include all components for rest, gRPC, workers etc.
    """

    app_component_classes: list[type[AppBaseComponent]] = []

    @classmethod
    def add_app_component_class(cls, component_class: type[AppBaseComponent]):
        cls.app_component_classes.append(component_class)
