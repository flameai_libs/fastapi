
# Библиотека для преднастроенного приложения FastAPI

## Пример использования:

```python
from common.fastapi.app import App

app = App()
```

при использовании должны подключиться общепринятые настройки (ручки здоровья, настройки воркеров и т.д.):

requirements.txt

```text
--extra-index-url https://gitlab.com/api/v4/projects/flameai_libs%2Fpypi/packages/pypi/simple common-fastapi=={версия}
```

## Линтинг кода и код-стайл:

Установка для разработки

```sh
pip install -e ".[dev]"
```

Поднятие версии

```sh
bump-my-version bump {major/minor/patch} setup.py --tag --commit
```


Форматирование:

```sh
make format
```

Проверка:

```sh
make linting
```
