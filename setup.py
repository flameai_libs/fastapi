from setuptools import setup


setup(
    name="common_fastapi",
    description="My customized library for using FastAPI framework",
    version="v0.1.1",
    author="Alexander Andryukov",
    author_email="andryukov@gmail.com",
    install_requires=[
        "fastapi==0.103.0",
        "uvicorn==0.23.2",
    ],
    extras_require={
        "dev": ["bump-my-version==0.20.0", "ruff==0.3.5"],
        "tests": ["pytest==8.1.1"],
    },
)
